1 Tema do Sistema

Sistema de Gestão de Campeonato de Futebol


2 Descrição Geral do Produto

I. Lista de requisitos funcionais

R01 – O sistema deve manter informações cadastrais de todos os atletas que disputam o torneio.

R02 – O sistema deve fazer a gestão da tabela do campeonato, detalhando informações de jogos
ganhos, perdidos e empates. Além disso deve ser controlado a quantidade de gols feitos, sofridos e
saldo de gols.

R03 – O sistema deve manter para quais campeonatos são cedidas vagas automáticas, dada a
classificação no campeonato.

R04 – O sistema deve manter informações cadastrais de todas as equipes que disputam o torneio.

R05 – O sistema deve fazer a gestão de cada rodada do campeonato.

R06 – O sistema deve construir a tabela do campeonato.

R07 – O sistema deve permitir que os torcedores visualizem os resultados dos jogos por rodada, ou por
equipe.

R08 – O sistema deve manter informações cadastrais de todos os locais habilitados para realizar
partidas do campeonato.

R09 – O sistema deve manter informações cadastrais de todos os árbitros que podem apitar jogos do
campeonato.

R10 – O torcedor pode visualizar o placar, local do jogo e árbitros dos jogos.


II. Regras de negócio inicialmente identificadas

RN00 – Quantidade de Atletas por time
Descrição: Cada time deve ter 24 atletas inscritos.

RN01 – Pontuação dos jogos
Descrição: Time vencedor ganha 3 pontos, Time perdedor não soma pontos, e em caso de empate
cada time ganha 1 ponto.

RN02 – Quantidade de times e rodadas no campeonato
Descrição: o Campeonato deve ter 20 equipes e 19 rodadas.

RN03 – Definição das rodadas.
Descrição: Antes do inicio do campeonato as rodadas devem ser definidas.
Cada rodada deve ter 10 jogos, sendo os confrontos definidos de forma aleatória. Todas as
equipes se enfrentam apenas 1 vez durante o campeonato.
O sistema deve construir as 24 rodadas de forma sequencial, ou seja, a segunda rodada só
pode ser sorteada após o fim do sorteio da rodada 1 e assim sucessivamente até construir as 24
rodas. Não podem ocorrer confrontos repetidos dentro do campeonato.
O local da partida também deve ser sorteado, e pode ser realizado somente na cidade de um
dos 2 times envolvidos na partida.

RN04 – Quantidade árbitros por partida
Descrição: Cada partida deve ter escalado 4 árbitros.


3 Desenvolvimento do Sistema

1) Crie um repositório no GitLab para o desenvolvimento do trabalho.

2) Configure as permissões para que todos os membros do grupo possam colaborar nesse
projeto.

3) O Sistema deve ser entregue em duas versões.
a. A Versão 1.0 que deve conter apenas 5 dos requisitos identificados.
b. A Versão 2.0 deve adicionar ao sistema os demais requisitos.
c. O planejamento dos requisitos que irão compor cada uma das versões do sistema faz
parte do escopo do trabalho.

4) Deve ser construído um documento com a definição dos requisitos que irão compor cada
umas das versões do sistema. Esse documento deve conter também o planejamento da
criação das branches e tags (baselines) necessárias para o trabalho. Esse documento deve
ser entregue junto com a versão 1.0 do sistema.

5) A Versão 1.0 deve ser desenvolvida na branch MASTER, e deve ser entregue em:
14/11.

6) A Versão 2.0 do sistema deve ser desenvolvida na branch ENTREGA2, e deve ser
entregue em 01/12.

7) A branch ENTREGA2 deve ser criada a partir da branch MASTER. O melhor momento para
criação dessa branch faz parte do escopo do trabalho, portanto deve ser planejado pelo
grupo.

8) A gestão das branches durante as entregas é responsabilidade do grupo. Utilizem boas
práticas de uso do GIT, realizem commit e push com frequência para minimizar as
dificuldades de merge.

9) A branch ENTREGA2 deve ser reintegrada a branch MASTER antes da entrega.
10)Para cada entrega deve ser gerada uma tag a partir da branch MASTER com o nome
VERSAO_X.X (as tags devem ser geradas no dia da entrega)

11) A entrega do trabalho deve ser feita nas datas especificadas:
a. Versão 1.0: 14/11
b. Versão 2.0: 01/12

12) Na primeira entrega deve ser postado no classroom o documento com o planejamento das
versões do sistema e a URL do projeto no GitLab. Na segunda entrega irei verificar o
mesmo repositório, portanto não precisa realizar nenhuma entrega pelo classroom.
