package dao;

import database.Conexao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import entidades.Jogador;

public class DAOJogador {
    
    public static void inserir(Connection c, Jogador t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" insert into jogador (nome, posicao, numeroCamisa, escalado) ");
        sql.append(" values (?, ?, ?, ?) ");

        Conexao.executeUpdate(c, sql.toString(),
                  t.getNome(), t.getPosicao(), 
                t.getNumeroCamisa(), t.isEscalado());
    }

    public static void excluir(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" delete from jogador ");
        sql.append(" where id = ? ");

        Conexao.executeUpdate(c, sql.toString(), id);
    }

    public static Jogador obter(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome, posicao, numeroCamisa, escalado from jogador ");
        sql.append(" where id = ? ");

        try (ResultSet rs = Conexao.executeQuery(c, sql.toString(), id)) {

            if (rs.next()) {
                return new Jogador(rs);
            } else {
                return null;
            }
        }
    }

    public static void alterar(Connection c, Jogador t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" update jogador ");
        sql.append(" set nome = ?, posicao = ?, numeroCamisa = ?, escalado = ? ");
        sql.append(" where id = ? ");

        Conexao.executeUpdate(c, sql.toString(),  
                t.getNome(), t.getPosicao(), 
                t.getNumeroCamisa(), t.isEscalado(),
                t.getId());
    }

    public static List<Jogador> lista(Connection c, String filtro) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome, posicao, numeroCamisa, escalado from jogador ");
        sql.append(" order by nome ");

        List<Jogador> l = new ArrayList<>();

        try (ResultSet rs = Conexao.executeQuery(c, sql.toString())) {
            while (rs.next()) {
                l.add(new Jogador(rs));
            }
        }
        return l;
    }
}