package dao;

import database.Conexao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import entidades.Arbitro;

public class DAOArbitro {
    
    public static void inserir(Connection c, Arbitro t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" insert into arbitro (nome, uf, assistentes, escalado) ");
        sql.append(" values (?, ?, ?, ?) ");

        Conexao.executeUpdate(c, sql.toString(),
                t.getNome(), t.getUf(), 
                t.getAssistentes().toString(), t.isEscalado());
    }

    public static void excluir(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" delete from arbitro ");
        sql.append(" where id = ? ");

        Conexao.executeUpdate(c, sql.toString(), id);
    }

    public static Arbitro obter(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome, uf, assistentes, escalado from arbitro ");
        sql.append(" where id = ? ");

        try (ResultSet rs = Conexao.executeQuery(c, sql.toString(), id)) {

            if (rs.next()) {
                return new Arbitro(rs);
            } else {
                return null;
            }
        }
    }

    public static void alterar(Connection c, Arbitro t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" update arbitro ");
        sql.append(" set nome = ?, uf = ?, assistentes = ?, escalado = ? ");
        sql.append(" where id = ? ");

        Conexao.executeUpdate(c, sql.toString(),
                t.getNome(), t.getUf(), 
                t.getAssistentes().toString(), t.isEscalado(),
                t.getId());
    }

    public static List<Arbitro> lista(Connection c, String filtro) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome, uf, assistentes, escalado from arbitro ");
        sql.append(" order by nome ");

        List<Arbitro> l = new ArrayList<>();

        try (ResultSet rs = Conexao.executeQuery(c, sql.toString())) {
            while (rs.next()) {
                l.add(new Arbitro(rs));
            }
        }
        return l;
    }
}