package dao;

import database.Conexao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import entidades.Campeonato;

public class DAOCampeonato {
    
    public static void inserir(Connection c, Campeonato t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" insert into campeonato (nome, quantidadeTimes, quantidadeVagaAutomaticas, quantidadeRodadas, ");
        sql.append(" timesDisputando, arbitrosApitando) ");
        sql.append(" values (?, ?, ?, ?, ?, ?) ");

        Conexao.executeUpdate(c, sql.toString(),
                t.getNome(), t.getQuantidadeTimes(), t.getQuantidadeVagaAutomaticas(), 
                t.getQuantidadeRodadas(), t.getTimesDisputando().toString(), t.getArbitrosApitando());
    }

    public static void excluir(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" delete from campeonato ");
        sql.append(" where id = ? ");

        Conexao.executeUpdate(c, sql.toString(), id);
    }

    public static Campeonato obter(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome, quantidadeTimes, quantidadeVagaAutomaticas, quantidadeRodadas, ");
        sql.append(" timesDisputando, arbitrosApitando from campeonato ");
        sql.append(" where id = ? ");

        try (ResultSet rs = Conexao.executeQuery(c, sql.toString(), id)) {

            if (rs.next()) {
                return new Campeonato(rs);
            } else {
                return null;
            }
        }
    }

    public static void alterar(Connection c, Campeonato t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" update campeonato ");
        sql.append(" set nome = ?, quantidadeTimes = ?, quantidadeVagaAutomaticas = ?, quantidadeRodadas = ? ");
        sql.append(" set timesDisputando = ?, arbitrosApitando = ? ");
        sql.append(" where id = ? ");

        Conexao.executeUpdate(c, sql.toString(),
                t.getNome(), t.getQuantidadeTimes(), t.getQuantidadeVagaAutomaticas(), 
                t.getQuantidadeRodadas(), t.getTimesDisputando().toString(), t.getArbitrosApitando(),
                t.getId());
    }

    public static List<Campeonato> lista(Connection c, String filtro) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome, quantidadeTimes, quantidadeVagaAutomaticas, quantidadeRodadas, ");
        sql.append(" timesDisputando, arbitrosApitando from campeonato ");
        sql.append(" order by nome ");

        List<Campeonato> l = new ArrayList<>();

        try (ResultSet rs = Conexao.executeQuery(c, sql.toString())) {
            while (rs.next()) {
                l.add(new Campeonato(rs));
            }
        }
        return l;
    }
}