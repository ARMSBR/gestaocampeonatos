package dao;

import database.Conexao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import entidades.VagasCampeonato;

public class DAOVagasCampeonato {
    
    public static void inserir(Connection c, VagasCampeonato t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" insert into vagascampeonato (idCampeonatoDisputado, quantidadeVagas, quantidadeVagasAutomaticas, ");
        sql.append(" times, campeonatoVagasAutomaticas) ");
        sql.append(" values (?, ?, ?, ?, ?) ");

        Conexao.executeUpdate(c, sql.toString(),
                  t.getCampeonatoDisputado(), t.getQuantidadeVagas(), 
                t.getCampeonatoVagasAutomaticas(), t.getTimes().toString(), t.getCampeonatoVagasAutomaticas());
    }

    public static void excluir(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" delete from vagascampeonato ");
        sql.append(" where id = ? ");

        Conexao.executeUpdate(c, sql.toString(), id);
    }

    public static VagasCampeonato obter(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select idCampeonatoDisputado, quantidadeVagas, quantidadeVagasAutomaticas, ");
        sql.append(" times, campeonatoVagasAutomaticas from vagascampeonato ");
        sql.append(" where id = ? ");

        try (ResultSet rs = Conexao.executeQuery(c, sql.toString(), id)) {

            if (rs.next()) {
                return new VagasCampeonato(rs);
            } else {
                return null;
            }
        }
    }

    public static void alterar(Connection c, VagasCampeonato t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" update vagascampeonato ");
        sql.append(" set idCampeonatoDisputado = ?, quantidadeVagas = ?, quantidadeVagasAutomaticas = ?, times = ? , campeonatoVagasAutomaticas = ? ");
        sql.append(" where id = ? ");

        Conexao.executeUpdate(c, sql.toString(),
                t.getCampeonatoDisputado(), t.getQuantidadeVagas(), 
                t.getCampeonatoVagasAutomaticas(), t.getTimes().toString(), t.getCampeonatoVagasAutomaticas(),
                t.getId());
    }

    public static List<VagasCampeonato> lista(Connection c, String filtro) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select idCampeonatoDisputado, quantidadeVagas, quantidadeVagasAutomaticas, ");
        sql.append(" times, campeonatoVagasAutomaticas from vagascampeonato ");
        sql.append(" order by nome ");

        List<VagasCampeonato> l = new ArrayList<>();

        try (ResultSet rs = Conexao.executeQuery(c, sql.toString())) {
            while (rs.next()) {
                l.add(new VagasCampeonato(rs));
            }
        }
        return l;
    }
}