package dao;

import database.Conexao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import entidades.Clube;

public class DAOClube {
    
    public static void inserir(Connection c, Clube t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" insert into clube (nome, uf, estadio, equipes) ");
        sql.append(" values (?, ?, ?, ?) ");

        Conexao.executeUpdate(c, sql.toString(),
                  t.getNome(), t.getUf(), 
                t.getEstadio(), t.getEquipe().toString());
    }

    public static void excluir(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" delete from clube ");
        sql.append(" where id = ? ");

        Conexao.executeUpdate(c, sql.toString(), id);
    }

    public static Clube obter(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome, uf, estadio, equipes from clube ");
        sql.append(" where id = ? ");

        try (ResultSet rs = Conexao.executeQuery(c, sql.toString(), id)) {

            if (rs.next()) {
                return new Clube(rs);
            } else {
                return null;
            }
        }
    }

    public static void alterar(Connection c, Clube t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" update clube ");
        sql.append(" set nome = ?, uf = ?, estadio = ?, equipes = ? ");
        sql.append(" where id = ? ");

        Conexao.executeUpdate(c, sql.toString(),
                t.getNome(), t.getUf(), 
                t.getEstadio(), t.getEquipe().toString(),
                t.getId());
    }

    public static List<Clube> lista(Connection c, String filtro) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome, uf, estadio, equipes from clube ");
        sql.append(" order by nome ");

        List<Clube> l = new ArrayList<>();

        try (ResultSet rs = Conexao.executeQuery(c, sql.toString())) {
            while (rs.next()) {
                l.add(new Clube(rs));
            }
        }
        return l;
    }
}