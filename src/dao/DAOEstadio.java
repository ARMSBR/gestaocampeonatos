package dao;

import database.Conexao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import entidades.Estadio;

public class DAOEstadio {

  private int id;
    private String nome;
    private String logradouro;
    private int numero;
    private String complemento;
    private String bairro;
    private String cidade;
    private String uf;
    private String cep;
    private int capacidade;
    
    public static void inserir(Connection c, Estadio t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" insert into estadio (nome, logradouro, numero, complemento, ");
        sql.append(" bairro, cidade, uf, cep, capacidade) ");
        sql.append(" values (?, ?, ?, ?, ?, ?, ?, ?, ?) ");

        Conexao.executeUpdate(c, sql.toString(),
                t.getNome(), t.getLogradouro(), 
                t.getNumero(), t.getComplemento(),
                t.getBairro(), t.getCidade(), 
                t.getUf(), t.getCep(),
                t.getCapacidade());
    }

    public static void excluir(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" delete from estadio ");
        sql.append(" where id = ? ");

        Conexao.executeUpdate(c, sql.toString(), id);
    }

    public static Estadio obter(Connection c, int id) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome, logradouro, numero, complemento, ");
        sql.append(" bairro, cidade, uf, cep, capacidade from estadio ");
        sql.append(" where id = ? ");

        try (ResultSet rs = Conexao.executeQuery(c, sql.toString(), id)) {

            if (rs.next()) {
                return new Estadio(rs);
            } else {
                return null;
            }
        }
    }

    public static void alterar(Connection c, Estadio t) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" update estadio ");
        sql.append(" set nome = ?, logradouro = ?, numero = ?, complemento = ? ");
        sql.append(" set bairro = ?, cidade = ?, uf = ?, cep = ? ");
        sql.append(" set capacidade = ?");
        sql.append(" where id = ? ");

        Conexao.executeUpdate(c, sql.toString(),  
                t.getNome(), t.getLogradouro(), 
                t.getNumero(), t.getComplemento(),
                t.getBairro(), t.getCidade(), 
                t.getUf(), t.getCep(),
                t.getCapacidade(),
                t.getId());
    }

    public static List<Estadio> lista(Connection c, String filtro) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome, logradouro, numero, complemento, ");
        sql.append(" bairro, cidade, uf, cep, capacidade from estadio ");
        sql.append(" order by nome ");

        List<Estadio> l = new ArrayList<>();

        try (ResultSet rs = Conexao.executeQuery(c, sql.toString())) {
            while (rs.next()) {
                l.add(new Estadio(rs));
            }
        }
        return l;
    }
}