package entidades;

import java.sql.ResultSet;
import java.util.Arrays;
import java.util.LinkedList;

public class Clube {
	private int id;
	private String nome;
	private String uf;
	private String estadio;
        private LinkedList<Jogador> equipe;
        
        public Clube() 
        {
    }

    public Clube(ResultSet rs) throws Exception {
        this.id = rs.getInt("id");
        this.nome = rs.getString("nome");
        this.uf = rs.getString("uf");
        this.estadio = rs.getString("estadio");
        this.equipe = new LinkedList(Arrays.asList(rs.getString("equipes").split(",")));
    }

	public int getId(){
		return this.id;
	}

	public void setId(int id){
		this.id = id;
	}

	public String getNome(){
		return this.nome;
	}

	public void setNome(String nome){
		this.nome = nome;
	}

	public String getUf(){
		return this.uf;
	}

	public void setUf(String uf){
		this.uf = uf;
	}

	public String getEstadio(){
		return this.estadio;
	}

	public void setEstadio(String estadio){
		this.estadio = estadio;
	}

    public LinkedList<Jogador> getEquipe() {
        return equipe;
    }

    public void setEquipe(LinkedList<Jogador> equipe) {
        this.equipe = equipe;
    }
}
