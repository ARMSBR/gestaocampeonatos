package entidades;

// Título equivale à entidade, seja ela um time, uma equipe ou qualquer forma

import java.sql.ResultSet;
// coletiva para jogadores.
public class Jogador {
    private int id;
    private String nome;
    private String posicao;
    private int numeroCamisa;
    private boolean escalado;
    
      public Jogador() 
        {
    }

    public Jogador(ResultSet rs) throws Exception {
        this.id = rs.getInt("id");
        this.nome = rs.getString("nome");
        this.posicao = rs.getString("uf");
        this.numeroCamisa = rs.getInt("numeroCamisa");
        this.escalado = rs.getBoolean("escalado");
    }
    
    public int getId(){
        return this.id;
    }

    public void setId(int id){
           this.id = id;
    }

    public String getNome(){
            return this.nome;
    }

    public void setNome(String nome){
            this.nome = nome;
    }

    public String getPosicao(){
            return this.posicao;
    }

    public void setPosicao(String posicao){
            this.posicao = posicao;
    }

    public int getNumeroCamisa(){
            return this.numeroCamisa;
    }

    public void setNumeroCamisa(int numeroCamisa){
            this.numeroCamisa = numeroCamisa;
    }

    public boolean isEscalado() {
        return escalado;
    }

    public void setEscalado(boolean escalado) {
        this.escalado = escalado;
    }
}
