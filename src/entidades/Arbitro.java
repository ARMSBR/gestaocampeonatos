/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.sql.ResultSet;
import java.util.Arrays;
import java.util.LinkedList;

/**
 *
 * @author 71501126
 */
public class Arbitro {
    
    int id;
    String nome;
    String uf;
    LinkedList<String> assistentes;
    boolean escalado;
    
        public Arbitro() 
        {
    }

    public Arbitro(ResultSet rs) throws Exception {
        this.id = rs.getInt("id");
        this.nome = rs.getString("nome");
        this.uf = rs.getString("uf");
        this.assistentes = new LinkedList(Arrays.asList(rs.getString("assistentes").split(",")));
        this.escalado = rs.getBoolean("escalado");
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public LinkedList<String> getAssistentes() {
        return assistentes;
    }

    public void setAssistentes(LinkedList<String> assistentes) {
        this.assistentes = assistentes;
    }

    public boolean isEscalado() {
        return escalado;
    }

    public void setEscalado(boolean escalado) {
        this.escalado = escalado;
    }
}
