package entidades;

import java.sql.ResultSet;
import java.util.Arrays;
import java.util.LinkedList;

public class Classificacao {
    
    private int id;
    private int idCampeonato;
    private LinkedList<Clube> clubeClassificados;
    private LinkedList<Campeonato> campeonatoVagasAutomaticas;
    
    public Classificacao() 
        {
    }

    public Classificacao(ResultSet rs) throws Exception {
        this.id = rs.getInt("id");
        this.idCampeonato = rs.getInt("campeonato");
        this.clubeClassificados = new LinkedList(Arrays.asList(rs.getString("clubeClassificados").split(",")));
        this.campeonatoVagasAutomaticas = new LinkedList(Arrays.asList(rs.getString("campeonatoVagasAutomaticas").split(",")));
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCampeonato() {
        return idCampeonato;
    }

    public void setCampeonato(int idCampeonato) {
        this.idCampeonato = idCampeonato;
    }

    public LinkedList<Clube> getClubeClassificados() {
        return clubeClassificados;
    }

    public void setClubeClassificados(LinkedList<Clube> clubeClassificados) {
        this.clubeClassificados = clubeClassificados;
    }

    public LinkedList<Campeonato> getCampeonatoVagasAutomaticas() {
        return campeonatoVagasAutomaticas;
    }

    public void setCampeonatoVagasAutomaticas(LinkedList<Campeonato> campeonatoVagasAutomaticas) {
        this.campeonatoVagasAutomaticas = campeonatoVagasAutomaticas;
    }	
}
