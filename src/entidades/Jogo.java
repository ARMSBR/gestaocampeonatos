/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.security.Timestamp;
import java.util.Date;

/**
 *
 * @author 71501126
 */
public class Jogo {
    private Date data;
    private Timestamp duracao;
    private Clube anfitriao;
    private Clube visitante;
    private int golAnfitriao;
    private int golVisitante;
    private Arbitro arbitro;
    private Estadio estadio;

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
    
    public Timestamp getDuracao() {
        return duracao;
    }

    public void setDuracao(Timestamp duracao) {
        this.duracao = duracao;
    }

    public Clube getAnfitriao() {
        return anfitriao;
    }

    public void setAnfitriao(Clube anfitriao) {
        this.anfitriao = anfitriao;
    }

    public Clube getVisitante() {
        return visitante;
    }

    public void setVisitante(Clube visitante) {
        this.visitante = visitante;
    }

    public int getGolAnfitriao() {
        return golAnfitriao;
    }

    public void setGolAnfitriao(int golAnfitriao) {
        this.golAnfitriao = golAnfitriao;
    }

    public int getGolVisitante() {
        return golVisitante;
    }

    public void setGolVisitante(int golVisitante) {
        this.golVisitante = golVisitante;
    }

    public Arbitro getArbitro() {
        return arbitro;
    }

    public void setArbitro(Arbitro arbitro) {
        this.arbitro = arbitro;
    }

    public Estadio getEstadio() {
        return estadio;
    }

    public void setEstadio(Estadio estadio) {
        this.estadio = estadio;
    }
    
    
}
