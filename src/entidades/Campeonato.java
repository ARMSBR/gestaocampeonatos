package entidades;

import java.sql.ResultSet;
import java.util.Arrays;
import java.util.LinkedList;

public class Campeonato {
    private int id;
    private String nome;
    private int quantidadeTimes;
    private int quantidadeVagaAutomaticas;
    private int quantidadeRodadas;
    private LinkedList<Clube> timesDisputando;
    private LinkedList<Arbitro> arbitrosApitando;

     public Campeonato() 
    {
    }

    public Campeonato(ResultSet rs) throws Exception {
        this.id = rs.getInt("id");
        this.nome = rs.getString("nome");
        this.quantidadeTimes = rs.getInt("quantidadeTimes");
        this.quantidadeVagaAutomaticas = rs.getInt("quantidadeVagaAutomaticas");
        this.quantidadeRodadas = rs.getInt("quantidadeRodadas");
        this.timesDisputando = new LinkedList(Arrays.asList(rs.getString("timesDisputando").split(",")));
        this.arbitrosApitando = new LinkedList(Arrays.asList(rs.getString("arbitrosApitando").split(",")));
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQuantidadeTimes() {
        return quantidadeTimes;
    }

    public void setQuantidadeTimes(int quantidadeTimes) {
        this.quantidadeTimes = quantidadeTimes;
    }

    public int getQuantidadeVagaAutomaticas() {
        return quantidadeVagaAutomaticas;
    }

    public void setQuantidadeVaga(int quantidadeVagaAutomaticas) {
        this.quantidadeVagaAutomaticas = quantidadeVagaAutomaticas;
    }

    public int getQuantidadeRodadas() {
        return quantidadeRodadas;
    }

    public void setQuantidadeRodadas(int quantidadeRodadas) {
        this.quantidadeRodadas = quantidadeRodadas;
    }

    public LinkedList<Clube> getTimesDisputando() {
        return timesDisputando;
    }

    public void setTimesDisputando(LinkedList<Clube> timesDisputando) {
        this.timesDisputando = timesDisputando;
    }

    public LinkedList<Arbitro> getArbitrosApitando() {
        return arbitrosApitando;
    }

    public void setArbitrosApitando(LinkedList<Arbitro> arbitrosApitando) {
        this.arbitrosApitando = arbitrosApitando;
    }
}
