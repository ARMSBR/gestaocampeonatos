

package entidades;

import java.sql.ResultSet;
import java.util.Arrays;
import java.util.LinkedList;

public class VagasCampeonato {

    private int id;    
    private int idCampeonatoDisputado;
    private int quantidadeVagas;
    private int quantidadeVagasAutomaticas;
    private LinkedList<Clube> times;
    private LinkedList<Integer> campeonatoVagasAutomaticas;
    
     public VagasCampeonato() 
    {
    }

    public VagasCampeonato(ResultSet rs) throws Exception {
        this.id = rs.getInt("id");
        this.idCampeonatoDisputado = rs.getInt("quantidadeTimes");
        this.quantidadeVagas = rs.getInt("quantidadeVagaAutomaticas");
        this.quantidadeVagasAutomaticas = rs.getInt("quantidadeRodadas");
        this.times = new LinkedList(Arrays.asList(rs.getString("times").split(",")));
        this.campeonatoVagasAutomaticas = new LinkedList(Arrays.asList(rs.getString("campeonatoVagasAutomaticas").split(",")));
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
   
    public int getIdCampeonatoDisputado() {
        return idCampeonatoDisputado;
    }

    public void setIdCampeonatoDisputado(int idCampeonatoDisputado) {
        this.idCampeonatoDisputado = idCampeonatoDisputado;
    }

    public LinkedList<Integer> getCampeonatoVagasAutomaticas() {
        return campeonatoVagasAutomaticas;
    }

    public void setCampeonatoVagasAutomaticas(LinkedList<Integer> campeonatoVagasAutomaticas) {
        this.campeonatoVagasAutomaticas = campeonatoVagasAutomaticas;
    }

    public int getCampeonatoDisputado() {
        return idCampeonatoDisputado;
    }

    public void setCampeonatoDisputado(int idCampeonatoDisputado) {
        this.idCampeonatoDisputado = idCampeonatoDisputado;
    }

    public int getQuantidadeVagas() {
        return quantidadeVagas;
    }

    public void setQuantidadeVagas(int quantidadeVagas) {
        this.quantidadeVagas = quantidadeVagas;
    }

    public int getQuantidadeVagasAutomaticas() {
        return quantidadeVagasAutomaticas;
    }

    public void setQuantidadeVagasAutomaticas(int quantidadeVagasAutomaticas) {
        this.quantidadeVagasAutomaticas = quantidadeVagasAutomaticas;
    }

    public LinkedList<Clube> getTimes() {
        return times;
    }

    public void setTimes(LinkedList<Clube> times) {
        this.times = times;
    }
}
